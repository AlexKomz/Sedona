const gulp = require("gulp");
const plumber = require("gulp-plumber");
const fileinclude = require("gulp-file-include");
const htmlmin = require("gulp-htmlmin");
const uglifyes = require("gulp-uglify-es").default;
const sourcemap = require("gulp-sourcemaps");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const groupmedia = require("gulp-group-css-media-queries");
const csso = require("gulp-csso");
const rename = require("gulp-rename");
const imagemin = require("gulp-imagemin");
const webp = require("gulp-webp");
const svgstore = require("gulp-svgstore");
const del = require("del");
const sync = require("browser-sync").create();


// Images

const images = () => {
  return gulp.src("source/img/**/*.{jpg,png,svg}")
    .pipe(imagemin([
      imagemin.optipng({ optimizationLevel: 3 }),
      imagemin.mozjpeg({ progressive: true }),
      imagemin.svgo()
    ]));
}

exports.images = images;


// Sprite

const sprite = () => {
  return gulp.src("source/img/**/s-icon-*.svg")
    .pipe(svgstore())
    .pipe(rename("sprite.svg"))
    .pipe(gulp.dest("source/img"));
}

exports.sprite = sprite;


// Webp

const webpImg = () => {
  return gulp.src("source/img/**/*.{jpg,png}")
    .pipe(webp({ quality: 70 }))
    .pipe(gulp.dest("build/img/webp"));
}

exports.webpImg = webpImg;


// Styles

const styles = () => {
  return gulp.src("source/sass/style.scss")
    .pipe(plumber())
    .pipe(sourcemap.init())
    .pipe(
      sass({
        outputStyle: "expanded"
      })
    )
    .pipe(groupmedia())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: true
      })
    )
    .pipe(sourcemap.write("."))
    .pipe(gulp.dest("build/css"))
    .pipe(csso())
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest("build/css"))
    .pipe(sync.stream());
}

exports.styles = styles;


// Scripts

const scripts = () => {
  return gulp.src("source/js/**/*.js")
    .pipe(plumber())
    .pipe(fileinclude())
    .pipe(gulp.dest("build/js"))
    .pipe(uglifyes())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest("build/js"))
    .pipe(sync.stream());
}

exports.scripts = scripts;


//Html

const html = () => {
  return gulp.src([
    "source/*.html",
    "!source/_*.html"
  ], {
    base: "source"
  })
    .pipe(fileinclude())
    .pipe(
      htmlmin({ collapseWhitespace: true })
    )
    .pipe(gulp.dest("build"))
    .pipe(sync.stream());
}

exports.html = html;


//Copy

const copy = () => {
  return gulp.src([
    "source/fonts/**/*.{woff,woff2}",
    "source/img/**",
    "!source/img/**/sprite.svg",
    "!source/img/**/s-icon-*.svg"
  ], {
    base: "source"
  })
    .pipe(gulp.dest("build"));
}

exports.copy = copy;


//Clean

const clean = () => {
  return del("build");
}

exports.clean = clean;


//Build

const build = gulp.series(
  clean,
  images,
  copy,
  webpImg,
  sprite,
  styles,
  scripts,
  html
)

exports.build = build;


// Server

const server = (done) => {
  sync.init({
    server: {
      baseDir: 'build'
    },
    cors: true,
    notify: false,
    ui: false,
  });
  done();
}

exports.server = server;


// Watcher

const watcher = () => {
  gulp.watch("source/js/*.js", gulp.series("scripts"));
  gulp.watch("source/sass/**/*.scss", gulp.series("styles"));
  gulp.watch("source/**/*.html", gulp.series("html"));
  gulp.watch("build/*.html").on("change", sync.reload);
}

exports.default = gulp.series(
  build, server, watcher
);
