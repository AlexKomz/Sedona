/* main-nav start */

var navMain = document.querySelector('.main-nav');
var navOpenBtn = document.querySelector('.main-nav__open-button');
var navCloseBtn = document.querySelector('.main-nav__close-button');

navMain.classList.remove('main-nav--nojs');

navMain.classList.remove('main-nav--opened');
navMain.classList.add('main-nav--closed');

navOpenBtn.addEventListener('click', function () {
  if (navMain.classList.contains('main-nav--closed')) {
    navMain.classList.remove('main-nav--closed');
    navMain.classList.add('main-nav--opened');
  } else {
    navMain.classList.remove('main-nav--opened');
    navMain.classList.add('main-nav--closed');
  }
});

navCloseBtn.addEventListener('click', function () {
  if (navMain.classList.contains('main-nav--opened')) {
    navMain.classList.remove('main-nav--opened');
    navMain.classList.add('main-nav--closed');
  }
});

/* main-nav end */

